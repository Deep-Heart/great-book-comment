---
title: 最好的我们 (八月长安) 书评
img: >-
  https://cdn.jsdelivr.net/gh/Deep-Heart/picx-images-hosting@master/boomments/最好的我们.1abi5zvbl23k.webp
categories: 现代文学
tags:
  - 青春成长
  - 友情
  - 爱情
  - 八月长安
keywords:
  - 青春
  - 友情
  - 爱情
  - 成长
  - 校园小说
summary: 耿耿余淮
abbrlink: 53979
date: 2024-06-15 00:00:00
---

[![封面](https://cdn.jsdelivr.net/gh/Deep-Heart/picx-images-hosting@master/boomments/最好的我们.1abi5zvbl23k.webp)]()
### 电子书下载地址
[最好的我们 (八月长安) .mobi](https://url57.ctfile.com/f/23765157-1272466768-ae7f0c)
![](https://cdn.jsdelivr.net/gh/Deep-Heart/picx-images-hosting@master/WeChat/wechat_mp_large.6xheshb4rok0.webp)
<center>关注公众号<font color="#ff0000">《好书评》</font>，回复<font color="#ffc000">“豆瓣图书250”</font>，即刻获得下载密码</center>

### 内容简介
你总是说青春从不曾永远，而那时候的我们，就是最好的我们。

这一次，我们和整个青春做告别。

八月长安全新力作。

你还记得高中时的同桌吗？那个少年有世界上最明朗的笑容，那个女生有世界上最好看的侧影。高中三年，两个人的影子和粉笔灰交织在一起，黑白分明，在记忆里面转圈。本书以怀旧的笔触讲述了女主角耿耿和男主角余淮同桌三年的故事，耿耿余淮，这么多年一路走过的成长故事极为打动人心，整个故事里有的都是在成长过程中细碎的点点滴滴，将怀旧写到了极致，将记忆也写到了极致。

### 作者简介
八月长安：

昵称二熊，哈尔滨长大，北京读书，现居上海。

个人微博：http://weibo.com/bayuechangan

代表作：

“振华高中三部曲”

《你好，旧时光》三册装典藏版

《暗恋·橘生淮南》两册装典藏版（预计2013年9月出版）

《最好的我们》两册装（2013年8月出版）

“改编电影小说”

《被偷走的那五年》（2013年8 月出版）

时光机——贴图你被偷走的最好时光http://www.douban.com/online/11599025/

### 读书笔记
其实早就说好了的。从第一次写《橘生淮南》的书评时就跟她保证，她出多少书我写多少评。现在第三本书终于出来了，她在讲故事的路上越走越远，而我已经在镜子中窥见自己的江郎才尽。
文艺女青年都有变成单身女中年的一天，毋须因为这种事情特别伤感。毕竟，没有人能青春不老，也甚少有人能够和她一样，有颗不老的心。
我们都是平凡人，和耿耿一样。
人人自有心头好，振华三部曲的三个女生里，耿耿是离我心理状态最远的那个——可能因为不理解她的孤勇，能为了一个男生就弃文从理；可能因为嫉妒她的年轻，年轻到她喜欢的人恰好喜欢她；更可能只是单纯地因为我五行缺贱，最看不得这样正能量的姑娘……
谁知道呢，这世上有太多八字不合。

1、我们在用一生的时间努力学会接受平庸

《最好的我们》是振华系列里面最宽厚的一本书。
林杨和周周，洛枳和盛淮南，都是得天独厚的人。聪明、勤奋、甚至美貌……他们出生前，一定曾经被上帝亲吻许久，不然那些别人千辛万苦才能谋得一星半点的优势，为什么会自然而然地长在他们身上，还美其名曰——天赋。？
未免偏心得，太明显了些。
我第一次开始真正意义上接受自己的平庸，大约就是大学的第一个学期。那时的本人秉着不求最好但求尽力的态度学习了半个学期的高等数学B，却在期中考试里面差点不及格；反观平时几个不怎么上课的、竞赛保送的男生，一个个轻轻松松满不在乎地拿了满分。
那时候的自己尚未脱离以考试分数为唯一生命质量标准的人生阶段，说不郁闷，都是假的。
按照一个励志的故事脉络，我应该以此为耻、奋发图强、悬梁刺股、夙兴夜寐并成功地在下一次考试中考出好成绩狠狠回给这个世界一个耳光以解我心头之恨。
然而我没有。
或许因为我是个没什么信念的人，所以乖乖地接受了自己的“智力在那个变态学院里大约就是个中等偏下的水准，应该过一些正常人的日子”的人物剧情设定。
当然，这也可能是我的借口。
我向来很喜欢拿“智商压制”当成借口。

我们会从生命的某一刻起开始逐渐接受自己的平庸。这一刻或早或晚，可能是去幼儿园的第一天你发现别人比你长得高；可能是你刚刚会ABC的时候已经有同龄人会说Bonjour；可能是你好不容易美化过的简历跟别人一比几乎空白；可能是你到了一个职位之后发现身边的人弃若敝履；可能是一直觉得自己是个富二代的你发现跟真的有钱人比起来你堪称贫穷……
我们为此纠结痛苦不甘怨怼，但大部分人、绝大部分人，还是逐渐地低调地调试自己的定位。
生活从来不曾打磨我们，它只是让我们在人生这个并不算舒适的座位上不断调整自己的坐姿罢了。
所以，即使我不喜欢，我依然十分敬佩耿耿。一个人屡战屡败是很容易的事情，但是，像我，可能很容易地就会接受自己的设定，甘于平凡，或是早早离场，断没有屡败屡战地决心和毅力。
我不敢说孰好孰坏，就像我也不知道，你们是否和我一样。

2、你是什么时候发现你爱他？

你是什么时候发现，爱上一个人的？
是他在球场上的一个潇洒的跳跃或是一个拉风的过人，你被阳光闪瞎了眼欢呼震聋了耳朵，心跳那一刻停止，从此眼神再也离不开？
或者，他坐在你身边，笔尖在学校自制的卷子或是人教版的教科书上划过，带来轻微的沙沙的声响，你在文山题海里面蓦地抬头，忽然觉得这是最让你觉得沉静的画面？
再或是，他走路的时候不小心撞到了擦得锃亮的玻璃门、骑自行车时有意耍帅双手不握扶手却马失前蹄地栽倒在地，你感同身受不忍直视，自己都替他觉得疼，但在看见他揉揉屁股拍拍灰一跃而起的时候，又不自觉地扯出八颗牙的弧度？
……
好感总是很容易发觉，那是近似于夏天吃了冰淇淋、冬天捧着烤红薯一样直接的观感。可是我想问的是，你是什么时候发现自己真的喜欢他？
那种抓心挠肝、辗转反侧、非卿不可的喜欢。
或许、应该、是你感到嫉妒的时刻。
就像那个会关心余淮排名的耿耿，那个一直相信余淮的优秀的耿耿，那个一直希望余淮能够一直快乐的耿耿，终究还是在文潇潇出现时开始不可控制地尖酸，才会在陈雪君的事情上失去理智的脑补。
我从来讨厌所谓“大爱”的论调。爱是狭隘且自私的，离开了自己，对方过得越好，只能印证自己的无足轻重。
无足轻重的你，真的会开心么？

3、说不定我一生涓滴意念侥幸汇成河

我有多不喜欢这本书的结局，就有多深爱这本书的结尾。

二熊在微博上为《耿耿余淮》征集出版名的时候，我出于自己对大小姐坑品和效率的了解，并没有轻易跳坑。但是想帮忙的心情是实在的，于是忝颜问她：讲的是啥故事？
她回答的轻描淡写：同桌的你，没在一起。
言简意赅。
于是当我看完最终章的时候我第一反应就是给她发短信：我不甘心，为什么她们都那么幸福！
她很快回给我：哈哈哈你可以去加入情侣去死团了哈哈哈。
倒也不是什么情侣去死团，只是好像年纪大了，就越来越理解，情海生波阴差阳错才是人生的普遍状态，花好月圆都是歌词里唱给别人听的古老桥段。
二十郎当岁是多微妙的年纪，可能一分之差就天南海北，可能一次契机就天翻地覆。我们指天誓日友谊长存的时候未尝不是掏心掏肺，但是大多数人生如蝼蚁，很可能的，一次分别便是永不相见。
大家不妨摸着胸口的大面包或者小馒头反问，一路走来，我们丢了多少朋友。
世界上那么多有缘无分的情侣相忘于江湖直至面目模糊，却为什么，唯独这两只这么幸运？

看到结局的时候我正坐在一个嘈杂的饭店里，陪家里的几个长辈吃饭。他们聊他们的，我谎称自己要看工作的文件，对着ipad屏一言不发。
所以大家应该能理解，我在看到“盛淮南爱洛枳，全世界都知道”那一句时瞬间泪奔的德行，着实吓坏了家里人。
只好慌忙地解释：太累了、太累了，最近压力有点大。
那是北京的七月份，空调房外是明晃晃地晒得人睁不开眼的阳光。窗内人声鼎沸窗外车来车往，第一次带我来这家店的朋友那时已经因缘际会地断了联系，手机号码都被从通讯录里面删除。
然而在狗血的生活外，有人的念念不忘还是会有回响。
就像那天吃过饭后我开车载大家遛长安街的时候，音乐频道正在播李宗盛的《山丘》。
老男人在里面唱：
说不定我一生涓滴意念侥幸汇成河
然后我俩各自一端 望着大河弯弯
终于敢放胆
嘻皮笑脸 面对 人生的难

我们妥协我们退缩，我们接受无奈的结果，我们面目模糊我们将错就错，时间久了似乎这样也可以一样过。可是心里面会怨怼会犹豫，会在不相信幸运的同时期待幸运。
就像我和朋友说，我们已经到了身边的人一个也丢不起的年纪；
就像朋友和我说，不能低头不能丢脸的关系实在不能长久，人这辈子哪能不出糗；
就像我们都认同的，出糗出习惯了也就没心理压力了；
就像我们拉钩说好的，要对彼此的关系死皮赖脸。

人生大河弯弯，那么容易就被冲散，要是没你们陪伴，哪有勇气，面对人生的难。
