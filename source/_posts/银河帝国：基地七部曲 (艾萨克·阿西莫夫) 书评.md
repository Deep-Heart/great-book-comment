---
title: 银河帝国：基地七部曲 (艾萨克·阿西莫夫) 书评
img: >-
  https://cdn.jsdelivr.net/gh/Deep-Heart/picx-images-hosting@master/boomments/银河帝国：基地七部曲.2329gne06q0w.webp
categories: 科幻小说
tags:
  - 未来世界
  - 政治阴谋
  - 社会变革
  - 艾萨克·阿西莫夫
keywords:
  - 银河帝国
  - 基地七部曲
  - 科幻小说
summary: 阿西莫夫经典科幻小说
abbrlink: 63280
date: 2024-06-15 00:00:00
---

[![封面](https://cdn.jsdelivr.net/gh/Deep-Heart/picx-images-hosting@master/boomments/银河帝国：基地七部曲.2329gne06q0w.webp)]()
### 电子书下载地址
[银河帝国：基地七部曲 ([美] 艾萨克·阿西莫夫 [未知]) .epub](https://url57.ctfile.com/f/23765157-1272466837-097983)
![](https://cdn.jsdelivr.net/gh/Deep-Heart/picx-images-hosting@master/WeChat/wechat_mp_large.6xheshb4rok0.webp)
<center>关注公众号<font color="#ff0000">《好书评》</font>，回复<font color="#ffc000">“豆瓣图书250”</font>，即刻获得下载密码</center>

### 内容简介
人类蜗居在银河系的一个小角落——太阳系，在围绕太阳旋转的第三颗 行星上，生 活了十多万年之久。    人类在这个小小的行星（他们称之为“地球”）上，建立了两百多个不同的行政区域（他们称之为“国家”），直到地球上诞生了第一个会思考的机器人。    在机器人的帮助下，人类迅速掌握了改造外星球的技术，开启了恢弘的星际殖民运动；人类在银河系如蝗虫般繁衍扩张，带着他们永不磨灭的愚昧与智慧、贪婪与良知，登上了一个个荒凉的星球，并将银河系卷入漫长的星际战国时代，直至整个银河被统一，一个统治超过2500万个住人行星、疆域横跨十万光年、总计数兆亿人口的庞大帝国崛起——银河帝国。    一个微妙的转折发生在银河帝国建国后的12020年。哈里·谢顿，这个刚满32岁的年轻数学家，开创了“心理史学”，这门学科能用数学公式准确推演全人类的未来——“预言”从此成为一门可以信任的科学，人类由此可以看见未来。    谢顿的第一个预言是：虽然毫无征兆，但已存在一万两千年之久的银河帝国即将灭亡。    一时间，银河震动，帝国飘摇；皇帝、宰相、夺权者、反叛星球，各方势力立刻剑拔弩张，人类银河时代最伟大的传奇就此开启……

### 作者简介
艾萨克·阿西莫夫（1920-1992）    俄裔美籍作家，被全世界的读者誉为“神一样的人”；美国政府授予他“国家的资源与自然的奇迹”这个独一无二的称号，以表彰他在“拓展人类想象力”上作出的杰出贡献。    阿西莫夫是一个全知全能的作家，其著作几乎覆盖人类生活的一切方面，上天下海、 古往今来、从恐龙到亚原子到全宇宙无所不包，从通俗小说到罗马帝国史，从科普读物到远东千年历史，从圣经指南，到科学指南，到两性生活指南，每一部著作都朴实、严谨而又充满幽默风趣的格调，为了尽情发挥自己诙谐搞笑的天赋，他甚至还写过一本《笑话集》。    他提出的“机器人学三大法则”是当代机器人学的基本法则，他预言了今天的生物科技，预言了互联网时代的数字图书馆，预言了人类将进行太空殖民。    终其一生，阿西莫夫最引以为豪的则是：《银河帝国》系列小说。    --------------------------------    译者：叶李华    一九六二年生，台湾大学电机系毕业，加州大学柏克莱分校理论物理博士，致力推广中文科幻与通俗科学二十余年，相关著作与译作数十册。自一九九〇年起，即透过各种管道译介、导读及讲授阿西莫夫作品，被誉为“阿西莫夫在中文世界的代言人”。

### 读书笔记
神奇的预言帝
阿西莫夫无疑是科幻界的鼻祖，他的代表作《银河帝国》，一直被认为是人类想象力的极限，人类历史上最有趣迷人的故事，预言了人类未来两万年的历史。但他的预言远不止于此。那么，阿西莫夫都预言过些什么呢？ 

“机器人三定律”
早在1950年，人类还不知道机器人是什么的时候，阿西莫夫就在《我，机器人》中提出了“机器人三定律”，席卷全球，最终成为当代机器人学的基本法则。

GIF

机器人的应用
阿西莫夫预言机器人将成为人类的得力助手。而如今，仓库、工厂、高科技办公室，机器人已经得到大规模使用。日本本田公司开发的目前世界上最先进的步行机器人以asimo命名，就是为了向阿西莫夫致敬。

GIF

万维网(1988)
阿西莫夫在1980年发表的《全球化电脑图书馆》，主题正是15年后的“万维网”。

生物科技
阿西莫夫在1954年发表的《钢穴》中预言了基因改造，在1987年发表的《奇妙的航行2》中预言了用纳米机器人进入人体血管做手术的可能性，在1988年发表的《化学工程的未来》中讨论了当今最热门的生物科技。他当年的预言，正在成为现实。

太空旅行
阿西莫夫在1951年预言了人类会通过各种手段在各种星球之间往返旅行。而如今，宇宙飞船也即将载人饱览太空景色。

太空殖民
阿西莫夫在1954年发表的《钢穴》中预言人类将进行太空殖民，而人类的火星殖民计划也在近几年取得进展。


智商超群的天才，曾参与原子弹爆炸试验
作为一名科幻作家被众所周知的阿西莫夫，何以能够如此准确的预言人类的未来？这和他自小就超群的智商不无关系。

普通人的智商一般是80分，阿西莫夫总在160左右，属于“天赋极高”之列。他是个超级灵童，多次跳级，考试总是第一，是当之无愧的学霸。

高中毕业时，阿西莫夫年仅15岁，之后入读美国哥伦比亚大学。没错，就是那个奶茶妹妹和刘强东相遇的哥伦比亚大学，王力宏、李云迪女朋友就读的哥伦比亚大学。

阿西莫夫一路读到博士，后来被征入了军队，他曾作为化学专家被送到瓦胡岛参加一项原子弹爆炸试验。

但阿西莫夫并没有比别人幸运，拥有开挂一样成功的一生。他两岁时便生了一场大病，差点死于肺炎。

阿西莫夫是俄罗斯裔美国人，父母是俄国犹太人。他生于俄罗斯的一个小村庄，家境十分普通。病痛、敏感的种族问题和世俗生活深刻地影响着他后来的创作，他将浓厚的人文情怀和对民主、科学、环保的关注倾注在作品中。

在俄罗斯的生活显然没有令他的家庭满意，他们想要谋求别的出路，经历一番曲折，终于在他母亲的哥哥的帮助下移居美国。

到达美国后，他们家开了一家糖果店维生，是非常普通的工薪阶层。在他念大学时，就迫切想要自己支付学费了。


癫狂的写作之路

初露头角
早在1931年，11岁的阿西莫夫便开始写作。但和所有初出茅庐的作者一样，他频繁遭遇退稿，在连续9次收到退稿以后，阿西莫夫终于在《惊奇故事》上发表了第一个科幻故事《逐出灶神星》，紧接着发表了《致命的武器》、《趋势》等作品，他用自己的才华征服了他的读者，从此有能力自己支付学费。

在哥大读研期间，成绩优异的他又在《超级科幻小说》上发表了《我，机器人》，并持续不断发表创作。


狂热的科幻迷
1941年，阿西莫夫发表了《日暮》，描述了一个被六个太阳照耀、从未遇到过日落的行星，突然发生了日蚀之后的故事。

1950年代，阿西莫夫持续为科幻杂志写短篇小说，他称这个时期是自己的“黄金十年”。《银河帝国》的许多篇目都是在此期间完成。

1966年，阿西莫夫因《基地三部曲》荣获了科幻小说界最高奖项“雨果奖”。

1972年，阿西莫夫发表了一篇描写外星人的科幻小说《神们自己》，并凭借这部作品获得了雨果奖和星云奖。

1982年10月，阿西莫夫出版了基地系列新作《基地边缘》，讲述了“基地三部曲”之后发生的故事。这部书获得了巨大成功，并获得了1983年雨果奖的最佳小说奖。

整个80年代，阿西莫夫坚持每个月在《幻想与科幻》杂志上发表专栏文章，直到病得根本不能动笔才被迫停止。


多面跨界能手
阿西莫夫除了是一位科幻狂人，他在跨界写作和演讲领域也收获了一众粉丝，这令他声名大噪。

阿西莫夫曾出版过一部生物化学教科书《生物化学与人体代谢》，这是他写作科普书籍的开始，后来出版的《聪明男人的科学指南》，在全国各地也受到了好评，并获得了国家图书奖的提名，他甚至出版过被当做工具书广泛使用的《阿西莫夫的科学与技术传记百科全书》，该书直至80年代仍反复再版。

阿西莫夫创作了包括《希腊人》、《罗马共和国》、《罗马帝国》、《埃及人》、《英格兰的形成》等在内的一系列历史题材书籍。他从法国历史一直写到了美国和君士坦丁堡的历史。同一时期，他甚至还出版了几部黄色打油诗集。

阿西莫夫在60年代成为了一名非常成功的非小说类作品创作者和公众演说家。


被放弃的生活：不写作就会死
科幻狂人阿西莫夫从俄罗斯的一个小村庄到美国，从不断被退稿的写作新人到跨界写作和演讲大师，在狂揽各大奖项和光鲜的背后，是他为了写作而被放弃的生活。

长年累月地坐在打字机前对身体非常不利，写作几乎让他失去了正常的生活，他的身体越来越差，第一次婚姻的破裂，也与他癫狂的写作状态有关。他曾在引言中写道：“给一位写作成瘾的作家当老婆，这种命运比死还悲惨。因为你的丈夫虽然身在家中，却经常魂不守舍。再没有比这种结合更悲惨的了。”

从80年代到90年代初，阿西莫夫的健康状况持续恶化，他的双手开始抖得无法操作打字机，只能口述，让别人帮他打出来。在生命的最后历程，阿西莫夫还与友人合作，将自己早期的短篇故事扩写成长篇小说。

阿西莫夫曾说：“我不为别的，只为写作而活着。”当有人问起阿西莫夫惊人的创作数量时，他总说自己是“不得不写，我写作的原因，如同呼吸一样，如果不这样做，我就会死去。”


粉丝收割机：阿西莫夫创造了一个完整的宇宙
阿西莫夫无疑是个粉丝收割机，他不仅征服了科幻迷们，也征服了世界范围内众多的科幻作家，甚至包括著名演员、经济学家、天文学家和主持人……

美国著名的天文学家兼科幻作家卡尔·萨根说：“在这个科技的世纪，我们需要一位能将科学和公众联系在一起的人，没有人能把这项工作做得像阿西莫夫那样出色，他是我们这个时代伟大的讲解员。”

阿西莫夫逝世后，他又在悼文中写道：“我们永远也无法知晓，究竟有多少第一线的科学家由于读了阿西莫夫的某一本书，某一篇文章，或某一个小故事而触发了灵感；也无法知晓有多少普通的公民因为同样的原因而对科学事业寄予深情……我并不为他而担忧，是为我们其余的人担心——我们身旁再也没有阿西莫夫激励年轻人奋发学习和投身科学了。”

2008年诺贝尔经济学奖得主保罗•克鲁格曼公开表示，他的经济学理论来自《银河帝国》的启示；热播美剧《生活大爆炸》主角谢尔顿(Sheldon)的名字就是在向《银河帝国：基地》的主角谢顿（Seldon）致敬；美国著名影星威尔·史密斯（Will Smith）（《我，机器人》男主角）称是阿西莫夫的概念说服了他选择出演此片；著名主持人蔡康永说阿西莫夫象征了一个开放、广阔的心灵，那是极其稀少的；《三体》作者刘慈欣说，阿西莫夫绝对是在中国拥有读者最多以及知名度最高的科幻作家。

阿西莫夫的作品从来不是简单的科幻小说，他在宏大的叙事场景内，包裹着他执着的科学精神、民主意识、环保主义……围绕时空旅行的主题创造了一个完整的宇宙，包括它的过去、现在和将来。


银河帝国：人类未来两万年历史预言

《阿西莫夫：银河帝国•基地》被誉为“人类历史上最好看的系列小说”，刘慈欣说，阿西莫夫的“基地系列”是他的代表作，在基地中，阿西莫夫第一次创作了“银河帝国”这一经典的科幻模型，“卢卡斯的星球大战”是基地系列的通俗版，这是恢弘壮阔的未来史，银河系的史诗，一部奠定现代科幻基石的经典。

无论是《星球大战》还是《阿凡达》，都借鉴了《银河帝国》中的内容和创意，甚至在9•11之后，英国《卫报》报道，本•拉登正是依据《银河帝国：基地》的战争策略，建立了同名恐怖组织——基地……

阿西莫夫的代表作《银河帝国》，是当之无愧的影响整个世界的科幻巨著。

出版六十年来，对人类的太空探索、世界局势、前沿经济学理论、好莱坞电影产生了深远的影响，更随着它的读者成长为各行各业的领袖（如美国总统小布什、诺贝尔奖获得者克鲁格曼、美国宇航局航天员、本•拉登），而将这种影响渗透到人类文化的方方面面。
