---
title: 职场进阶书单
abbrlink: 8428
date: 2024-06-12 23:57:50
top: true
categories: 书单
tags:
  - 职场
---
# 职场进阶书单

[刻意练习](https://book.jifou.cc/20240612841975720bce.html)

[底层逻辑](https://book.jifou.cc/20240612ba6b236ae729.html)

[斯坦福大学人生设计课](https://book.jifou.cc/20240612abb4807f420b.html)

[最重要的事，只有一件](https://book.jifou.cc/20240612da46dd2b8096.html)

[时间管理7堂课](https://book.jifou.cc/20240612b08ddc69e7a2.html)

[深度工作](https://book.jifou.cc/202406128030909829fd.html)

[规划最好的一年](https://book.jifou.cc/202406120d85a0a39a55.html)

[精力管理](https://book.jifou.cc/202406120d85a0a39a55.html)

[高效能人士 的七个习惯](https://book.jifou.cc/20240612d57d7a054643.html)
