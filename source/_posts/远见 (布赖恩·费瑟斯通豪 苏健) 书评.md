---
title: 远见 (布赖恩·费瑟斯通豪 苏健) 书评
img: >-
  https://cdn.jsdelivr.net/gh/Deep-Heart/picx-images-hosting@master/boomments/远见：如何规划职业生涯-3-大阶段.58h88jgof3.webp
categories: 文学作品
tags:
  - 家庭关系
  - 人生选择
  - 内心挣扎
  - 布赖恩·费瑟斯通豪 苏健
keywords:
  - 远见
  - 家庭
  - 梦想
  - 人性
abbrlink: 33968
date: 2024-06-19 00:00:00
summary: 如何规划职业生涯3大阶段
---

[![封面](https://cdn.jsdelivr.net/gh/Deep-Heart/picx-images-hosting@master/boomments/远见：如何规划职业生涯-3-大阶段.58h88jgof3.webp)]()
### 电子书下载地址
[远见：如何规划职业生涯 3 大阶段 (Brian Fetherstonhaugh , 苏健) .epub](https://url57.ctfile.com/f/23765157-1312044898-c78f30)
![](https://cdn.jsdelivr.net/gh/Deep-Heart/picx-images-hosting@master/WeChat/wechat_mp_large.6xheshb4rok0.webp)
<center>关注公众号<font color="#ff0000">《好书评》</font>，回复<font color="#ffc000">“豆瓣图书250”</font>，即刻获得下载密码</center>

### 内容简介
什么是成功的职业生涯？
成功的职业生涯不在于找到热爱的工作，而在于建立起你热爱的生活。
我们期盼梦想，但惧怕现实。我们认识的那个世界已经变了，而且变得很快。我们需要新的方法寻找工作，用新的方法建立可持续的职业生涯。
奥美互动全球首席执行官布赖恩•费瑟斯通豪在《远见》中，向我们展示了职业生涯中3个截然不同但相互关联的阶段，教会我们如何不断储备职场燃料以创造长期的成功。《远见》提供了实用的练习、工具和案例，带你重新思考和评估你的技能、时间和职场投资方向。远见思维帮助我们利用新方法寻找工作，平衡工作与生活，规划职业生涯的45年。

### 作者简介
布赖恩•费瑟斯通豪

● 奥美互动全球董事长兼首席执行官，奥美青年职业网络（Young Professional Network）执行发起人。担任善意实业董事会成员20多年，并且是多家初创公司的顾问。

● 营销专家，4E营销理论提出者。尤其擅长品牌建设、数字化营销，在过去的25年里，与各种大品牌，如IBM、美国运通、可口可乐、宜家、联合利华、雀巢等合作。获得“2014卡普尔斯•安迪•埃默森奖”（2014 Caples Andi Emerson Award）、“银苹果奖”（Silver Apple Award）等殊荣。

● 职业生涯导师和思想领袖。在近20年中，持续研究职业规划，在耶鲁大学、麻省理工学院、哈佛大学、哥伦比亚大学、麦吉尔大学和纽约大学等知名高校开展职业策略的讲座。

### 读书笔记
文/爱啃骨头的猫咪

你好，今天为你解读的书是《远见：如何规划职业生涯3大阶段》。这本书的中文版大约18万字，我会用大约15分钟的时间，为你讲述书中精髓：如何规划自己长达45年的职业生涯。

老龄化程度日益加深，几乎成了每个国家都必须面临的问题。采用延迟退休是各国应对人口老龄化的普遍做法。什么是延迟退休呢？就是延迟退休的年龄，再说通俗点就是，用法律规定增加你的工作年限。根据相关研究机构对全球170个国家或地区的退休年龄情况的调查，大概可以分成三种情况，第一种情况是发达国家普遍执行65岁以上的退休年龄，有的甚至达到了70岁，比如以色列；第二种情况是少数经济落后的小国，它们普遍执行的是低于60岁的退休年龄，他们这样做的原因或者是因为养老金制度不完善，覆盖人群有限，或者是因为人均预期寿命比较低；第三种情况是大多数发展中国家实行的60岁到65岁之间的退休年龄。

在中国，人力资源与社会保障部早在2015年就已经提出了制定延迟退休的政策。我们假设这个政策在2022年开始正式施行，那么，所有出生在1980年以后的人，都会赶上新政。什么意思呢？举个例子，如果一个1980年出生的人从20岁开始工作，原本他可以60岁就能退休安享晚年，但是政策推出后，他要一直工作到65岁左右才能退休，就是说他的工作时间有45年之久！这是一个几乎长达半个世纪的职业生涯，那么，你有没有想过，怎么才能让你在这45年的职业生涯中保持向上的状态并取得成功？你该如何创造差异发挥优势让自己脱颖而出？你又怎么能在临近退休甚至是退休之后继续发光发热为别人创造价值？先不着急回答，听听今天给你讲的这本书《远见：如何规划职业生涯3大阶段》，听完以后，没准儿你就能找到这些问题的答案了。

这本书的作者是奥美互动全球董事长兼首席执行官布赖恩•费瑟斯通豪。他在近20年的时间里持续地研究职业规划，在耶鲁大学、麻省理工学院、哈佛大学、哥伦比亚大学、麦吉尔大学和纽约大学等知名高校开展过职业策略的讲座。作者布赖恩在《远见》这本书中把职业生涯按照时间划分成了3个阶段，每个阶段分别持续大约15年的时间，并且作者认为，这3个阶段之间是紧密关联的，你在一个阶段的所作所为都可能为接下来的阶段创造机会，当然也可能是带来恶果。

好了，介绍完这本书的基本情况和作者概况，那么下面，我就为你具体讲解一下我们该怎么规划职业生涯的3个阶段。

先来看看职业生涯的第一个阶段你要如何规划。

作者布莱恩给出了八个字建议，“加添燃料，强势开局”。我给大家解释一下，职业生涯的前15年是一个强势开局的阶段，作为职业生涯的第一个阶段，并不是让你被动的增长年龄和阅历，而是有着强烈的目的性的阶段。在这个阶段，你唯一的目标就是为接下来的两个阶段打好基础，建立良好的习惯。而整个第一阶段，就是一个学习和探索的过程，为了找出你擅长什么、不擅长什么、喜欢什么以及不喜欢什么，你需要不断的尝试和犯错。司徒慕德伯爵说过：“职业生涯绝不是一条笔直上升的路线。我们常常需要为了前进而后退，为了变得更好而变得更坏，为了获得进步而投入资本。”什么意思呢？就是说在你工作的这45年中，不可能总是向上的晋升、加薪，肯定会遇到各种各样的挫折和阻碍，而有时候你的妥协和后退，其实是为了能更好的前进。所以，作者在最后给出了在职业生涯第一个阶段的策略是：“步入职场、迎接新发现，并为前方的漫长旅程储备职场燃料。”

在这你可能又有疑问了，什么是职场燃料呢？你最应该储备的职场燃料都有什么呢？不急，听我慢慢说。

作者布莱恩说，在职业生涯中存在三种基本形式的职场燃料，一号燃料是可迁移技能，二号燃料是有意义的经验，三号燃料是持久的关系。下面，我一个个的讲。

一号燃料，可迁移技能。什么是可迁移技能呢？你可以这么理解，就是你在职业生涯中获取并拥有的各种基本技能。这些技能不仅是帮助你完成眼前工作的技术知识和行业术语，而且是当你从一个工作换到另一个工作，从一家公司换到另一家公司，甚至是从一个行业换到另一个行业时，都能依靠的能力和基础。下面我就举6个确实能让你与别人拉开差距的可迁移技能的例子。准备好你的纸笔哦。

第一个，解决问题的能力。从某种程度上来说，世界上任何一份工作都是为了解决某个问题而存在的。你能够分析问题并且制定解决方案吗？当你面对一项挑战时，你是否有一两套能解决问题的可靠方法呢？在任何一份工作中，你都应该有意识地学习别人解决问题的方法，他们是如何切入这个问题，又是如何思考的？你要多收集不同的方法并试着将几种不同的方法结合起来，创造出适合你自己的独门秘方。

第二个，说服式沟通技巧。无论你最后进入哪个行业，说服力都是一种受用一生的关键技能。不管你的交流对象是客户、同事、朋友还是陌生人，能够把自己的观点用清晰、简洁的方式呈现出来都是一种基本技能。所以，不管你是通过书面形式表达自己，还是录制一段在线视频，哪怕你只是做一个小型的演讲，用对方能够听得懂并且能戳中他们痛点的文字和语言，让他们发自内心的赞同你的观点，你就赢了。

第三个，完成任务的能力。执行并完成任务的能力虽然再基础不过，但是对于长达45年的职业生涯来说具有巨大的价值。尽管每个人都具备一定的执行力，但是只有那些能不畏艰难、持续产出的人才能真正的脱颖而出。

第四个，吸引人才的能力。有一种说法是，拥有最优秀人才的公司通常都能成功。与它对应的一句话是，有能力吸引和调动尖端人才的个人领袖通常都能成功。将优秀的人才招揽到你的身边，让你的工作做得更好，首先你必须具备正确的思维方式，换句话说就是你要认识到没有人需要为你工作，必须是他们想要为你工作。所以，你能否吸引人才，就要看你自己本身的能力，比如你能让工作变得更具挑战性和乐趣吗？你能推动员工进步吗？你能平等并且透明的对待所有人吗？仔细思考一下。

第五个，帮助和求助的能力。在畅销书《沃顿商学院最受欢迎的成功课》中，亚当·格兰特通过观察三种社交风格，将它们与工作业绩和幸福指标关联起来，发现成功的“付出者”能让你在生意和生活中都更有效率，而付出超过收获的人，更容易在最杰出和最幸福的行列排名靠前。所以说，学会如何寻求帮助和如何提供帮助，会成为你在职场里一项强大的可迁移技能。

第六个，情商。什么是情商？情商就是你理解和连接他人情绪状态的能力。简单解释一下就是，你可以通过观察对方的肢体语言发现对方的情绪，是伤心还是愤怒。著名的情商领域的专家丹尼尔·戈尔曼在他的著作《情商3》中指出，影响业绩和优秀程度最重要的因素就是情商。对领导者而言，区分精英与普通人的标准几乎有百分之九十在于情商。

讲完一号燃料，在讲二号燃料之前我们再回顾一下可迁移技能都包括哪六个技能，第一个解决问题的能力，第二个说服式沟通技巧，第三个完成任务的能力，第四个吸引人才的能力，第五个帮助和求助的能力，最后一个情商。好了，下面我们看看二号燃料有意义的经验都包括什么呢？

有意义的经验结合起来可以让你在职业生涯中既成为复合型人才，又让你的职场奋斗之路变得稳健。人的一生会经历各种各样的事情，并且在这些经历中获得经验。对于职业生涯来说，那些有意义的职场经验会给你增加价值，让你能在职业发展上更加顺利。所以，无论是创业、跨国工作、第二语言，还是参加志愿者的经历，一系列有意义的经验都可以帮你建立起更加坚实的职业生涯。

最后，我们讲一下三号燃料持久的关系。进入职场之后，每个人的周围都会出现越来越多的关键人物和团体，他们强烈的影响着你的职业轨迹，并组成了一个职业生态系统。这个职业生态系统中包括，你的上司，你的客户，你的商业伙伴，你身边的人才，你的同事和下属等等可以给你带来帮助或者让你敬佩尊重的任何人。换个容易理解的词儿就是你的“人脉资源”。与你的人脉资源保持持久良好的关系，会给你的职业生涯带来意想不到的帮助。

好了，这就是今天给你讲的第一个内容：如何规划职业生涯的第一个阶段。我们来总结一下，在职业生涯的前15年，你应该带有强烈的目的性去学习和探索，找出你擅长什么、不擅长什么、喜欢什么以及不喜欢什么。你要为第二个第三个阶段准备充足的职场燃料，这些职场燃料包括可迁移技能，有意义的经验和持久的关系。在职业生涯第一个阶段，你的眼光要放长远一点，多去寻找或者创造能为你的职业生涯带来更多职场燃料的方法。

下面，我们来说职业生涯的第二个阶段你要如何规划。

作者布莱恩建议“锚定甜蜜区，聚焦长板”。我给大家具体讲解一下。职业生涯第二阶段大约从踏入职场的15年后开始，此时身在职场的你会感到独有的机会和焦虑：我还能取得更高的成就吗？我下一步要走向哪里？我在第一阶段中打下的基础能收获红利吗？我还能在日复一日的机械的工作中找到激情和动力吗？

不知道大家是否知道“木桶理论”呢？这个理论告诉你一个木桶能装多少水，取决于最短的那块板，如果有一块很短的板，其他板长到天上去，也没法装多少水。这个理论曾经在各个企业中非常受欢迎，但是随着全球化的发展，社会分工越来越明确，人们越来越认识到，对于什么都会一点的人永远都没有精通一门技术的人更受欢迎。一个人不可能精通所有的专业，也不可能补足自己所有的短板。所以，人们开始把更多的精力放在自己的最长的那根板子上，力求把一件事做到极致。

作者布莱恩也认为，职业生涯第二阶段是识别、拓展和投注你的长板的时候。这个时候，你必须学会拓展行动规模，把主要精力放在提高你的关键技能和核心长板上。

在职业生涯第一阶段，你关注的是培养专业知识、积累可迁技能，以及成为某些方面的咨询对象。那么在第二阶段你要做的就是，创造真正的差异，一旦找到任何可以让你“脱颖而出”的技能，你就需要投入时间培养它，直至精通它。而意愿和时间，就是创造精通技能的法宝。在《异类》中，马尔科姆·格拉德威尔研究了各个领域的佼佼者，发现大约需要一万小时的密集训练和演习，一个人才能在某一方面达到精通。这就是“一万小时天才理论”。

同时，我想你应该会很清楚，让自己在这么长的时间里沉浸在某个领域或某个问题中，就意味着不可避免的厌倦和沉闷。不断的练习、练习、再练习，很容易让你发疯。为了能够撑过这些时刻，你就不得不爱上这个领域，或者说你必须是真正的喜欢并热爱这个领域。不然的话，你很容易放弃。

我们可以举一个当代极好的例子，史蒂夫·乔布斯。他从小痴迷于技术和设计，在第一次加入苹果和后来在NeXT软件公司的日子里，他经历了很长一段学徒生涯，体验过许多的失败和无价的教训。当在1996年回归苹果公司时，他对潮流趋势的敏锐性几乎超过了所有人。找到你擅长的领域，投入高专注度和足够的时间，你就会精通任何一种技能。

作者布莱恩一开始就说过，一个人的职业生涯长达45年，我们不可能永远保持热情和活力对目标紧追不舍，职业生涯也不是一条笔直上升的路线。那么，我们怎么才能获得持续的成功呢？答案就是把眼光放长远一点，不要过于束缚自己，多去尝试更多的可能性，不要害怕失败，保持一直向前的动力，才有可能持续的成功。

在职业生涯第二阶段，你应该会成为一个公司或者一个部门的领导者，那么做为初出茅庐的管理者，你应该注意哪些问题呢？

作者布莱恩给初次当领导的领导者提了一些建议，我们来看一下。

第一条建议是，时刻注意你的仪容、态度和举止。什么意思呢，就是说你的员工会比过去更加仔细的观察你，你表现出来的快乐、压力、自信、失望等情绪都会被员工察觉，并调节他们自己的态度和行为。你的仪容、态度和举止正受到高度的关注和广泛的效仿。

第二条建议，简洁地表达你的愿景，并且不停的重复。作为领导，你需要寻找一些简单明了的词语，表达出你希望你的手下向着哪个方向前进。

再来看第三条建议，尽快选好团队成员。每个领导都需要一支小型的核心的团队，你要寻找那些能够高质量完成任务的亲密同事来组成，不要选择和你相似的人，要寻找那些能够增强你的长板和弥补你的短板的人，并且要一对一的深入了解他们。

第四条建议，每一个有意义的商业问题最好能在较小的团队中解决。每个人都是独立的个体，对同一个问题会有不同的意见，观点之间的对抗其实是有意义的，但作为领导要确保它发生在恰当的讨论中，不要放到严肃的大型公开的会议上讨论，这样解决不了问题反而会让加深同事之间的矛盾。

第五条建议，你要表现得像个被人信赖的解答者。对待每一个员工抱有最基本的公平，让消息在组织中透明的传播，不管好消息还是坏消息都告诉大家，帮助他们正确看待这些信息对他们的影响。

最后一条建议，你不需要无所不知，你要学会征询他人的意见。

好了，这就是今天给你讲的第二个内容：如何规划职业生涯的第二个阶段。我们来总结一下，第二阶段大约从踏入职场的15年后开始，如果说在第一阶段你是在寻找你擅长什么，那么第二阶段就是锚定它。你需要投入更多的精力和时间，精通一个技能，保持一直向前的动力，持续学习，并随时调整你的节奏。你对需要学习的技能了解的越多，就越能为职业生涯做出最佳的决策，从而将你获得长期成功的可能性最大化。作为一个领导，你要时刻注意自己的仪容、态度和举止对员工的影响，简洁明了的表达愿景，并不断的重复加深员工的理解，选好合适的团队成员，把每一个有意义的问题都在团队内部解决，让自己成为员工可以依靠和信赖的解答者，最后告诉自己你不是无所不知，你应该多找别人咨询。

最后，我们来看看职业生涯第三个阶段该怎么规划呢？

作者给出的建议是“优化长尾，发挥持续影响力”。是不是不太明白，没关系，听我慢慢说。

职业生涯第三个阶段，就是我们通常说的职业生涯后期，可以看做是临近退休和退休后的一段时间。职业生涯的后期不一定就是开始走下坡路，或者在退休那天遭遇突然的打击。作者布莱恩认为，合理规划后的第三阶段持续的时间超乎想象，而且回报也相当丰厚，关键就在于你是否能主动对这一阶段进行永不懈怠的塑造。

人类学家玛丽·凯瑟琳·贝特森在2015年的TEDx演讲中讲到，在过去的一个世纪里，发达经济体的人口寿命增加了整整30年。正如贝特森指出的那样，这并不是让“老年时期”延长了，而是让我们在变老之前额外增加了一个生命阶段。意思就是说，虽然你在生理年龄上被称为“老年人”，但是你的身体和精神都非常健康。在现实生活中我们也可以看到，很多60岁、70岁的人依旧在工作。因为对于大部分人的生活而言，工作是满足感和幸福的源泉，如果停止工作，就会感到无趣、失落甚至沮丧。

贝森特将50岁到85岁的这段时期看做是一个崭新的机会时代，而《100岁的人生策略》的作者琳达·格拉顿认为，职业生涯将会持续80年，期间将会发生多种多样的职业体验，以及持续一辈子的学习、行动和教育。这个观念跟我们中国人“活到老学到老”的思想很像。下面，我们来看看作者布莱恩有给出哪些建议，帮助你规划好职业生涯第三阶段呢。

第一个建议是，试验，自愿接受挑战。

我们来看看60岁的蒂姆·彭纳是如何规划自己第三阶段的职业生涯的。或许你会得到一些启发。

蒂姆在宝洁公司工作了30多年，最后以世界顶级快消品公司的领导者身份退休。在他50多岁的时候，他就开始考虑“接下来会如何”。告别了之前高强度的工作后，在他剩下的30甚至40年的预期寿命内，他开始意识到问题不在于离开某件事，而是要被吸引到其他的事情和目标上。由于对下一步并没有确切的想法，蒂姆开始尝试各种不同的工作，经过许许多多的试验，直到参与了一个大型募款行动，为社区活动筹到超过一亿美元的善款时，他才找到喜欢并适合的工作。后来55岁的他刚从宝洁正式退休，就成了大伦多地区基督教青年会的董事会主席。

从蒂姆的故事中我们可以看到，蒂姆对成长和学习保持着一种健康的观念，就是你要管理自己的学习曲线，要勇敢面对困难的问题，并自愿接受各种挑战。

布莱恩给的第二个建议是，创业，开辟全新疆域。

我们来认识一下纽约丝绸之路精品店的创始人兼总裁，休·派珀女士在职业生涯的第三阶段都做了什么。

休·派珀在80年代的IBM公司经历过一些困难的时期，这家雇佣了40多万人的公司遭遇了几次重大的变故，尽管公司幸存下来并继续兴盛的发展下去，但是在IBM工作的30多年里让休·派珀感觉是在给不同的公司干活儿。快要结束在IBM第三个10年的时候，她意识到自己在工作中的投入已经大不如前，于是做出了要离开的决定。在休·派珀52岁的时候去印度旅行，见到了各种织锦、纺织品之后让她开始了创业之路。如今，休·派珀创立的这家丝绸之路公司已经成为当地社区一个不可或缺的组成部分。

休·派珀虽然一路走来都很艰难，也受到了很多挑战，但是她依然保持对自己事业的热情，全身心的投入在工作中，尽管她已经快60岁了。

好了，我们来看看作者布莱恩的最后一个建议是制定退休计划，保持关联性。

对很多处在职业生涯第三阶段的人来说，退休是一个不可避免的事情，但是你要做的就是应该提前做好退休的计划。就像一列高速前进的火车，不能突然踩刹车停下，应该慢慢减速，不要一下子全都停下来，下车的时候至少音乐不要关掉。什么意思呢？就是说虽然你马上要退休了或者已经退休了，但是你积累的人脉、资源，你的技能、声誉都不会立刻消失，你应该好好制定一份计划，把专业能力和精通的技能传递给下一代。

I你也可以继续关注退休之前的工作行业新闻、论坛，参加相关的活动，与客户、之前的同事和后起之秀聊天，与他们交流和分享，继续保持你与他们的关联性，让自己与身处的环境与时俱进。职业生涯的第三阶段，对金钱、权利等的欲望都会逐渐降低，但对身体健康、对个人声誉有着更强烈的渴望。所以，制定好第三阶段的退休计划，与世界保持关联性，让自己更有成就感和满足感。

好，这就是今天给你讲的第三个内容：如何规划职业生涯的第三个阶段。我们来总结一下，职业生涯的第三阶段并不一定就是走下坡路的状态，它完全可以在你的努力和改变下成为多姿多彩的。做好自己的退休计划，保持自己的学习好奇心，管理自己的学习曲线，利用自己在第一、第二阶段所积累的专业技能和丰富经验，为更多的后起之秀提供帮助，让自己紧跟时代发展的脚步，与周遭世界保持关联性。好好计划你职业生涯的第三阶段，即使退休的日子不可避免，但是你继续学习的热情和精通的技能，会给你带来意想不到的回报。

好了，这本《远见：如何规划职业生涯3大阶段》就讲完了。最后，我们再简单回顾一下。

职业生涯第一阶段，作者建议“加添燃料，强势开局”。在职业生涯的前15年，你要为接下来的两个阶段打好基础，建立良好的习惯。为了找出你擅长什么、不擅长什么、喜欢什么以及不喜欢什么，你需要不断的尝试和犯错。同时，你要大量储备三种基本形式的职场燃料，这三种燃料包括可迁移技能、有意义的经验和持久的关系。

职业生涯第二阶段，作者建议“锚定甜蜜区，聚焦长板”。从踏入职场的15年后开始，你会感到独有的机会和焦虑，为了能从职场上脱颖而出，你需要找到你的核心长板，并花大量的时间把主要精力投入在上面，直到你精通它。同时，如果你在第二阶段晋升成为领导后，更需要注重自己的形象，时刻用一个领导者的标准严格要求自己。

职业生涯第三阶段，作者建议“优化长尾，发挥持续影响力”。随着人口寿命的增加，人们在退休后的时间越来越长，而对于大部分人的生活而言，工作是满足感和幸福的源泉，如果停止工作，就会感到无趣、失落甚至沮丧。所以，在职业生涯第三阶段，你应该提前开始制定退休计划，不管是去尝试新的职业还是创业，或者就想实现自己未曾实现的梦想，都需要你提前规划。并且，保持自己对学习的热情，让自己紧跟时代发展的脚步，与周遭世界保持关联性，这样即使是退休之后，你的生活也会是多姿多彩的。
